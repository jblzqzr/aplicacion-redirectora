#! usr/bin/python3.10
import socket
import random


def get_random_url():
    urls_dic = {"Google": "https://www.google.es/", "URJC": "https://www.urjc.es/",
                "Cursos Web": "https://www.cursosweb.github.io/",
                "Stack Overflow": "https://www.stackoverflow.com/", "Github": "https://www.github.com/"}
    rnd = random.randint(0, len(urls_dic) - 1)
    return list(urls_dic.keys())[rnd], list(urls_dic.values())[rnd]


def index(first_round):
    web_redirect, web_redirect_url = get_random_url()
    next_page = str(random.randint(0, 10000))
    next_url = "http://localhost:" + str(myPort) + "/" + next_page
    if first_round:
        http_code = "200 OK"
        first_round = False
    else:
        http_code = "301 Moved Permanently"

    html_body = "<h1>It works!</h1>" + '<p>Next page: <a href="' \
                + next_url + '">' + next_page + "</a></p>" + '<p>Next page: <a href="' \
                + web_redirect_url + '">' + web_redirect + "</a></p>"
    return http_code, html_body, first_round


if __name__ == "__main__":
    myPort = 1234
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    mySocket.bind(('', myPort))
    mySocket.listen(5)

    random.seed()
    try:
        firstRound = True
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            httpRequest = recvSocket.recv(2048)
            print("HTTP request received:")
            print(httpRequest)
            httpCode, htmlBody, firstRound = index(firstRound)

            # HTML body of the page to serve
            response = "HTTP/1.1 " + httpCode + "\r\n\r\n" + "<html><body>" + htmlBody + "</body></html>" + "\r\n"
            recvSocket.send(response.encode('ascii'))
            recvSocket.close()

    except KeyboardInterrupt:
        print("Closing binded socket")
        mySocket.close()
